/*global jQuery */
/* jshint browser: true */
jQuery(document).ready(function ($) {
    
    'use strict';
    
    function appendMenu() {

        if ($(window).width() < 770) {
            $('#menu-member-menu').appendTo('#menu-main-navigation');
        } else {
            $('#menu-member-menu').appendTo('.member-menu .ddl-navbar-collapse');
        }
    }
    
    $(document).ready(function (e) {
        appendMenu();
    });

    $(window).resize(function (e) {
        appendMenu();
    });

});