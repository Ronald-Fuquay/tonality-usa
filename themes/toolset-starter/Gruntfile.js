module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dev: {
				files: {
					'bootstrap/css/bootstrap.min.css' : 'bootstrap/sass/bootstrap.scss',
					"style.css" : "style.scss",
					'css/woocommerce.css' : 'css/woocommerce.scss',
					'css/theme.css' : 'css/theme.scss'
				}
			},
			build: {
				options: {
					style: 'compressed',
					sourcemap: 'none'
				},
				files: {
					'bootstrap/css/bootstrap.min.css' : 'bootstrap/sass/bootstrap.scss',
					"style.css" : "style.scss",
					'css/woocommerce.css' : 'css/woocommerce.scss',
					'css/theme.css' : 'css/theme.scss'
				}
			}
		},
		uglify: {
			build: {
				files: [{
					expand: true,
					cwd: 'js',
					src: ['**/*.js', '!**/*.min.js'],
					dest: 'js',
					ext: '.min.js',
					extDot: 'last'
				}]
			}
		},
		autoprefixer: {
			build: {
				files: {
					'style.css' : 'style.css',
					'bootstrap/css/bootstrap.min.css': 'bootstrap/css/bootstrap.min.css',
					'css/woocommerce.css' : 'css/woocommerce.css',
					'css/theme.css' : 'css/theme.css'
				}
			}
		},
		watch: {
			themecss: {
				files: ['*.scss', 'bootstrap/sass/*.scss', 'css/*.scss'],
				tasks: [['newer:sass:build'], ['newer:autoprefixer:build']]
			},
			themejs: {
				files: ['js/*.js', '!js/*.min.js'],
				tasks: ['newer:uglify:build']
			}
		},
		browserSync: {
			dev: {
				bsFiles: {
					src : [
						'**/*.css',
						'**/*.php'
					]
				},
				options: {
					watchTask: true,
					proxy: 'localhost'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.registerTask('default', ['browserSync','watch']);
};