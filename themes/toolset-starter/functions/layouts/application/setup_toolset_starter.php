<?php
class WPDDL_Integration_Setup_Toolset_Starter extends WPDDL_Theme_Integration_Setup_Abstract{


    protected function get_supported_theme_version(){
        return '1.3.5';
    }

    protected  function get_plugins_url( $relative_path ){
        return get_template_directory_uri() . '/functions/layouts';
    }

    protected function get_supported_templates() {
        return array(
            $this->getPageDefaultTemplate() => __( 'Page', 'ddl-layouts' )
        );
    }

    public function run() {
        parent::run();
        $this->setPageDefaultTemplate('page.php');
        return true;
    }

    public function hook_enqueue_frontend_scripts(){
        return;
    }
}
