<?php
/**
 *
 * $HeadURL: https://www.onthegosystems.com/misc_svn/crud/trunk_new/embedded/views/settings.php $
 * $LastChangedDate: 2015-03-03 15:08:45 +0100 (mar, 03 mar 2015) $
 * $LastChangedRevision: 32049 $
 * $LastChangedBy: francesco $
 *
 */
if (!defined('ABSPATH'))
    die('Security check');
if (!current_user_can(CRED_CAPABILITY)) {
    die('Access Denied');
}
$results = '';
$user_results = '';
$cred_import_file = null;
$show_generic_error = false;
if (isset($_POST['import']) && $_POST['import'] == __('Import', 'wp-cred') &&
        isset($_POST['cred-import-nonce']) &&
        wp_verify_nonce($_POST['cred-import-nonce'], 'cred-import-nonce')) {
    if (isset($_FILES['import-file'])) {
        $cred_import_file = $_FILES['import-file'];
        if ($cred_import_file['error'] > 0) {
            $show_generic_error = true;
            $cred_import_file = null;
        }
    } else {
        $cred_import_file = null;
    }

    if ($cred_import_file !== null && !empty($cred_import_file)) {
        $options = array();
        if (isset($_POST["cred-overwrite-forms"]))
            $options['overwrite_forms'] = 1;
        if (isset($_POST["cred-overwrite-settings"]))
            $options['overwrite_settings'] = 1;
        if (isset($_POST["cred-overwrite-custom-fields"]))
            $options['overwrite_custom_fields'] = 1;
        CRED_Loader::load('CLASS/XML_Processor');
        $results = CRED_XML_Processor::importFromXML($cred_import_file, $options);
    }
}

if (isset($_POST['import']) && $_POST['import'] == __('Import', 'wp-cred') &&
        isset($_POST['cred-user-import-nonce']) &&
        wp_verify_nonce($_POST['cred-user-import-nonce'], 'cred-user-import-nonce')) {
    if (isset($_FILES['import-file'])) {
        $cred_import_file = $_FILES['import-file'];

        if ($cred_import_file['error'] > 0) {
            $show_generic_error = true;
            $cred_import_file = null;
        }
    } else {
        $cred_import_file = null;
    }

    if ($cred_import_file !== null && !empty($cred_import_file)) {
        $options = array();
        if (isset($_POST["cred-overwrite-forms"]))
            $options['overwrite_forms'] = 1;
        if (isset($_POST["cred-overwrite-settings"]))
            $options['overwrite_settings'] = 1;
        if (isset($_POST["cred-overwrite-custom-fields"]))
            $options['overwrite_custom_fields'] = 1;
        CRED_Loader::load('CLASS/XML_Processor');
        $user_results = CRED_XML_Processor::importUserFromXML($cred_import_file, $options);
    }
}
?>
<div class="wrap">
    <?php screen_icon('cred-frontend-editor'); ?>
    
    <h2><?php _e('Import', 'wp-cred') ?></h2>
    <br />
    <!-- use WP Tabs here -->

    <br />    
    <a id="cred-import"></a>

    <form name="cred-import-form" enctype="multipart/form-data" action="" method="post">
        <?php wp_nonce_field('cred-settings-action', 'cred-settings-field'); ?>
        <table class="cred-widefat" id="cred_general_settings_table">
            <tbody>
                <tr>
                    <td><strong><?php _e('Import CRED Post Forms', 'wp-cred'); ?></strong></td>
                </tr>
                <tr>
                    <td>
                        <?php
                        if ($show_generic_error && isset($_POST['type']) && $_POST['type'] == 'post_forms') {
                            echo '<div style="color:red;font-weight:bold">' . __('Upload error or file not valid', 'wp-cred') . '</div>';
                        }
                        if (is_wp_error($results)) {
                            echo '<div style="color:red;font-weight:bold">' . $results->get_error_message($results->get_error_code()) . '</div>';
                        } elseif (is_array($results)) {
                            ?>
                            <ul style="font-style:italic">
                                <?php
                                /**
                                 * show settings imported message
                                 */
                                if ($results['settings']) {
                                    printf('<li>%s</li>', __('General Settings Updated', 'wp-cred'));
                                }
                                ?>
                                <li><?php _e('Custom Fields Imported', 'wp-cred'); ?> : <?php echo $results['custom_fields']; ?></li>
                                <li><?php _e('Forms overwritten', 'wp-cred'); ?> : <?php echo $results['updated']; ?></li>
                                <li><?php _e('Forms added', 'wp-cred'); ?> : <?php echo $results['new']; ?></li>
                            </ul>
                            <?php if (!empty($results['errors'])) { ?>
                                <ul>
                                    <?php foreach ($results['errors'] as $err) { ?>
                                        <li style="color:red;"><?php echo $err; ?></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>

                        <ul>
                            <li>
                                <label class='cred-label'><input id="checkbox-1" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-forms" value="1" /><span class='cred-checkbox-replace'></span>
                                    <span><?php _e('Overwrite existing post forms', 'wp-cred'); ?></span></label>
                            </li>
                            <input type="hidden" name="type" value="post_forms">
                            <!--<li>
                                <input id="checkbox-2" type="checkbox" name="cred-delete-other-forms"  value="1" />
                                <label for="checkbox-2"><?php //_e('Delete forms not included in the import','wp-cred');                                      ?></label>
                            </li>-->
                            <li>
                                <label class='cred-label'>
                                    <input id="checkbox-5" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-settings" value="1" />
                                    <span class='cred-checkbox-replace'></span>
                                    <span><?php _e('Import and Overwrite CRED Settings', 'wp-cred'); ?></span></label>
                            </li>
                            <li>
                                <label class='cred-label'>
                                    <input id="checkbox-6" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-custom-fields" value="1" />
                                    <span class='cred-checkbox-replace'></span>
                                    <span><?php _e('Import and Overwrite CRED Custom Fields', 'wp-cred'); ?></span></label>
                            </li>
                        </ul>
                        <label for="upload-cred-file"><?php __('Select the xml file to upload:&nbsp;', 'wp-cred'); ?></label>

                        <input type="file" class='cred-filefield' id="upload-cred-file" name="import-file" />

                        <input id="cred-import" class="button button-primary" type="submit" value="<?php echo esc_attr(__('Import', 'wp-cred')); ?>" name="import" />

                        <?php wp_nonce_field('cred-import-nonce', 'cred-import-nonce'); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

    <br />
    <br />
    <a id="cred-user-import"></a>
    <form name="cred-import-user-form" enctype="multipart/form-data" action="" method="post">
        <?php wp_nonce_field('cred-user-settings-action', 'cred-user-settings-field'); ?>
        <table class="cred-widefat" id="cred_general_settings_table">
            <tbody>
                <tr>
                    <td><strong><?php _e('Import CRED User Forms', 'wp-cred'); ?></strong></td>
                </tr>
                <tr>
                    <td>
                        <?php
                        if ($show_generic_error && isset($_POST['type']) && $_POST['type'] == 'user_forms') {
                            echo '<div style="color:red;font-weight:bold">' . __('Upload error or file not valid', 'wp-cred') . '</div>';
                        }
                        if (is_wp_error($user_results)) {
                            echo '<div style="color:red;font-weight:bold">' . $user_results->get_error_message($user_results->get_error_code()) . '</div>';
                        } elseif (is_array($user_results)) {
                            ?>
                            <ul style="font-style:italic">
                                <?php
                                /**
                                 * show settings imported message
                                 */
                                if ($user_results['settings']) {
                                    printf('<li>%s</li>', __('General Settings Updated', 'wp-cred'));
                                }
                                ?>
                                <?php if (false) { ?>
                                    <li><?php _e('Custom Fields Imported', 'wp-cred'); ?> : <?php echo $user_results['custom_fields']; ?></li>
                                <?php } ?>
                                <li><?php _e('User Forms overwritten', 'wp-cred'); ?> : <?php echo $user_results['updated']; ?></li>
                                <li><?php _e('User Forms added', 'wp-cred'); ?> : <?php echo $user_results['new']; ?></li>
                            </ul>
                            <?php if (!empty($user_results['errors'])) { ?>
                                <ul>
                                    <?php foreach ($user_results['errors'] as $err) { ?>
                                        <li style="color:red;"><?php echo $err; ?></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        <?php } ?>

                        <ul>
                            <li>
                                <label class='cred-label'><input id="checkbox-1" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-forms" value="1" /><span class='cred-checkbox-replace'></span>
                                    <span><?php _e('Overwrite existing user forms', 'wp-cred'); ?></span></label>
                            </li>
                            <input type="hidden" name="type" value="user_forms">
                            <!--<li>
                                <input id="checkbox-2" type="checkbox" name="cred-delete-other-forms"  value="1" />
                                <label for="checkbox-2"><?php //_e('Delete forms not included in the import','wp-cred');                                      ?></label>
                            </li>-->
                            <li>
                                <label class='cred-label'>
                                    <input id="checkbox-5" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-settings" value="1" />
                                    <span class='cred-checkbox-replace'></span>
                                    <span><?php _e('Import and Overwrite CRED Settings', 'wp-cred'); ?></span></label>
                            </li>
                            <?php if (false) { ?>                            <li>
                                    <label class='cred-label'>
                                        <input id="checkbox-6" type="checkbox" class='cred-checkbox-invalid' name="cred-overwrite-custom-fields" value="1" />
                                        <span class='cred-checkbox-replace'></span>
                                        <span><?php _e('Import and Overwrite CRED Custom Fields', 'wp-cred'); ?></span></label>
                                </li>
                            <?php } ?>
                        </ul>
                        <label for="upload-cred-file"><?php __('Select the xml file to upload:&nbsp;', 'wp-cred'); ?></label>

                        <input type="file" class='cred-filefield' id="upload-cred-file" name="import-file" />

                        <input id="cred-import" class="button button-primary" type="submit" value="<?php echo esc_attr(__('Import', 'wp-cred')); ?>" name="import" />

                        <?php wp_nonce_field('cred-user-import-nonce', 'cred-user-import-nonce'); ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>

</div>
